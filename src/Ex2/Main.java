package Ex2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Introduceti primul numar: ");
        double a = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Introduceti al doilea numar: ");
        double b = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Introduceti un semn de operare: ");
        String operator = scanner.nextLine();


        calculare(a, b, operator);

    }

    public static void calculare(double a, double b, String c) {
        if (c.equals("+")) {
            System.out.println(a + b);
        } else if (c.equals("-")) {
            System.out.println(a - b);
        } else if (c.equals("*")) {
            System.out.println(a * b);
        } else if (c.equals("/")) {
            System.out.println(a / b);
        } else {
            System.out.println("Nu este asa semn de operatie!");
        }

//        switch (c) {
//            case "+":
//                System.out.println(a + b);
//                break;
//            case "-":
//                System.out.println(a - b);
//                break;
//            case "*":
//                System.out.println(a * b);
//                break;
//            case "/":
//                System.out.println(a / b);
//                break;
//            default:
//                System.out.println("Nu este asa semn de operatie!");
//                break;
//        }
    }
}
